const express = require('express');
const cors = require('cors');

const app = express();
const port = 8083

// intentional delay
app.use((_req, _res, next) => {
    setTimeout(next, 1000);
});

app.use(cors())

app.get('/hello', (req, res) => {
    console.log(`received request with ${JSON.stringify(req.query)}`);

    let name = req.query.name ?? "nobody";
    name = name.replaceAll(" ", "+"); // since we use url parameters and send base64 encoded data we need to hack a little bit

    res.json({
        "name": name,
        "message": "Hello from API"
    });
})

app.listen(port, function () {
    console.log(`started api server on port ${port}`);
})
