const WebSocket = require('ws');

const port = 8082
const wss = new WebSocket.Server({port: port});

console.log(`started websocket server on port ${port}`)

wss.on('connection', (ws) => {

    ws.on('message', (data) => {
        console.log('received: %s', data);
    });

    const interval = setInterval(() => {
        ws.send(`👀 - there is another unicorn: 🦄`);
    }, 2000);

    ws.on('close', () => {
        console.log('closing')
        clearInterval(interval);
    });

    ws.send('hi dude!');
});
