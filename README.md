Prerequisites
- Node.js

Download dependencies
```bash
npm install
```

Start backend servers
```bash
npm start
```
